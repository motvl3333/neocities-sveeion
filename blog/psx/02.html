<!DOCTYPE html>
<html>
	<head>
		<title>02 - Texture mapping</title>
		<link rel="stylesheet" href="../../style.css">
		<link rel="icon" href="../../cube.ico">
	</head>

	<body>
		<header>
			<h1>02 - Texture mapping</h1>

			<div class="menu">
				<a href="/">Home</a>
				<a href="../../about.html">About</a>
				<a href="../../blog.html">Back</a>
			</div>
			<hr>
		</header>

		<main>
			<h6>March 3, 2019</h6>
			<div class="off_of_margin">
				<p class="tab">Although this took a while to accomplish, I finally rendered a texture onto multiple primitives. In this entry, I will describe how to do so.</p>
				<div class="center">
					<img src="../../images/blog/txmapping_screenshot.png" alt="image of textured primitives">
				</div>

				<p class="tab">Before I begin, let's look at some important concepts regarding the framebuffer.</p>

				<h2>The Framebuffer: An overview</h2>
				
				<h3>CLUTs (Color Look Up Tables)</h3>
				<p class="tab">A CLUT is simply an array of colors that are ultimately used by a texture. A CLUT can either have 16 entries, or 256 entries. All textures in a 4-bit mode texture page can only use 16 entries within any CLUT, while all textures in an 8-bit mode texture page use 256 entries within any CLUT. If you change the CLUT of a primitive, you're basically changing its color palette.</p>

				<h3>Drawing and Display Environments</h3>
				<p class="tab">The Drawing Environment holds information about drawing the next frame, and the Display Environment holds information about displaying the current frame. For example, I could create a custom Drawing Environment by declaring a <code>DRAWENV</code> structure and setting its dithering processing flag <code>dtd</code> to on:</p>

				<pre><code>
DRAWENV drawenv;
drawenv.dtd = 1;

PutDrawEnv(&amp;drawenv); // Make custom drawing environment the current one
				</code></pre>

				<p class="tab">You can also do things like change where the drawing area or display area is located inside of the framebuffer by changing certain members of your custom Drawing and Display Environments.</p>

				<h3>Texture Pages</h3>
				<p class="tab">A texture page is usually a 256x256px area within the framebuffer, with the framebuffer itself being 1024x512px in size. The size of a texture page may vary depending on the pixel format mode it is in:</p>
				<ul>
					<li>4-bit mode: 64x256px; textures use up to 16 entries in a CLUT</li>
					<li>8-bit mode: 128x256px; textures use up to 256 entries in a CLUT</li>
					<li>16-bit mode: 256x256px; direct RGB (does not use a CLUT)</li>
				</ul>
				<p class="tab">In essence, a texture page is where you store texture patterns and CLUTs into.</p>
				<div class="center">
					<img src="../../images/blog/the_framebuffer.png" alt="illustration of framebuffer" style="width:90%;height:90%">
				</div>

				<h3>Primitives</h3>
				<p class="tab">Here are the primitives I used:</p>

				<table align="center">
					<tr>
						<th>Name</th>
						<th>Description</th>
					</tr>

					<tr>
						<td>POLY_FT3</td>
						<td>A three sided polygon that uses flat shading and allows texture mapping.</td>
					</tr>

					<tr>
						<td>POLY_FT4</td>
						<td>A four sided polygon that uses flat shading and allows texture mapping.</td>
					</tr>

					<tr>
						<td>SPRT_16</td>
						<td>A 16x16 textured sprite.</td>
					</tr>

					<tr>
						<td>DR_MODE</td>
						<td>A special primitive that changes the <i>tpage</i>, <i>dtd</i>, <i>dfe</i>, and <i>tw</i> members of the drawing environment during drawing.</td>
					</tr>
				</table>

				<p class="tab">Now that that's out of the way, I'll describe how to render a POLY_FT4 primitive. The texture we'll be using is a 16x16px ball sprite that I retrieved from one of Psy-Q's sample programs. The sprite itself is a series of hex values defined like this:</p>
				<pre><code>
/* 4bit 16x16 ball pattern */
static u_long	ball16x16[] = {
	0x00000000,0x00000000,0x21000000,0x00000332,
	0x42110000,0x00055554,0x54321000,0x00677665,
	0x65432100,0x07888776,0x76543100,0x09998887,
	0x77654210,0x9aa99988,0x87765420,0xabbbaa99,
	0x98765420,0xbcccbba9,0x98876530,0xcddddcba,
	0xa9876530,0xdeeeddcb,0xa9887500,0x0effeddb,
	0xb9987500,0x0efffedc,0xba986000,0x00fffedc,
	0xba970000,0x000eeedc,0xa9000000,0x00000dcb
};
				</code></pre>

				<h2>Rendering a POLY_FT4 primitive</h2>
				<p class="tab">First, we'll intialize our primitive and set its location:<p>
				<pre><code>
POLY_FT4 ft4;

SetPolyFT4(&amp;ft4); // Initialize primitive
setRGB0(&amp;ft4, 100, 100, 100); // Set brightness
setXY4(&amp;ft4, 200, 100, 300, 100, 200, 200, 300, 200); // (x0, y0) - (x3, y3): top left -&gt; top right -&gt; bottom left -&gt; bottom right
				</code></pre>

				<h3>Loading a texture into the framebuffer</h3>

				<p class="tab">The POLY_FT4 and POLY_FT3 primitives have their own <code>tpage</code> member, which means that we can specify a texture page to load texture patterns from. <code>LoadTPage()</code> loads a specified texture into a given address in the framebuffer and returns the texture page ID. Let's load our ball16x16 texture into the framebuffer at (384, 0) and return the texture page in 4-bit mode:</p>
				<pre><code>
// u_short LoadTPage( u_long *pix,       Pointer to texture pattern
//                    int tp,            Bit depth (0 = 4-bit; 1 = 8-bit; 2 = 16-bit
//                    int abr,           Semitransparency rate
//                    int x, int y,      Destination frame buffer address
//                    int w, int h)      Texture pattern size

ft4.tpage = LoadTPage(ball16x16, 0, 0, 384, 0, 16, 16);
				</code></pre>

				<h3>Setting UV coordinates</h3>
				<p class="tab">Relative to the texture page, we specify UV coordinates where our primitive covers our 16x16 texture:</p>
				<pre><code>
setUV4(&amp;ft4, 0, 0, 16, 0, 0, 16, 16, 16);
				</code></pre>

				<h3>Using a CLUT</h3>
				<p class="tab">That Psy-Q sample program also provided us with a 2-dimensional array of CLUTs for our sprite pattern:</p> 
				
				<pre><code>
/* various CLUT for 4bit balls */
static u_long	ballcolor[][8] = {
	
	0x88670000,0x8cab8caa,0x94ef90cd,0x99329511,
	0x9d769954,0xa199a198,0xa5bba1ba,0xa5dda5dc,

	0x90c00000,0x99409520,0xa1a19d80,0xa601a5e1,
	0xae81aa41,0xb6c1b2a1,0xbb01b6e1,0xbf41bb21,
	
// (shortened for brevity)

	0x80c30000,0x81458124,0x81a68185,0x820781e7,
	0x82898248,0x82ca82aa,0x830b82eb,0x834c832c,
};
				</code></pre>
				
				<p class="tab">We'll only be using the first CLUT:</p>
				<pre><code>
LoadClut2(ballcolor[0], 384, 32); // Load a CLUT w/ 16 entries into the framebuffer at (384, 32)
ft4.clut = GetClut(384, 32);
				</code></pre>
				
				<h3>Putting it together:</h3>
				<pre><code>
POLY_FT4 ft4;

main()
{
	// Initialize quad
	SetPolyFT4(&amp;ft4);
	setRGB0(&amp;ft4, 100, 100, 100); // Set brightness
	setXY4(&amp;ft4, 200, 100, 300, 100, 200, 200, 300, 200) // top left -&gt; top right -&gt; bottom left -&gt; bottom right

	ft4.tpage = LoadTPage(ball16x16, 0, 0, 384, 0, 16, 16); // Load texture into framebuffer and return ID
	setUV4(&amp;ft4, 0, 0, 16, 0, 0, 16, 16, 16);
	ft4.clut = GetClut(384, 32);
	
	while(1)
	{
		DrawPrim(&amp;ft4);
	}
}
				</code></pre>
				
				<div class="center">
					<img src="../../images/blog/tx_poly_ft4.png">
				</div>
				
				<p class="tab">You can pretty much follow the steps above to render a POLY_FT3 primitive, but what about a SPRT_16? Well, a SPRT_16 primitive does not have its own <code>tpage</code> member, so rendering a SPRT_16 is a bit different.</p>
				
				<h2>Rendering a SPRT_16 with the DR_MODE primitive</h2>

				<p class="tab">Let's start by initializing our sprite and setting values to its members:</p>

				<pre><code>
SPRT_16 spr;

SetSprt16(&amp;spr);
setRGB0(&amp;spr);
setXY0(&amp;spr, 145, 200);

setUV0(&amp;spr, 0, 0); // Specify upper left coordinate of a 16x16 area
spr.clut = GetClut(384, 32); // We don't need to specify a texture page to retrieve a CLUT from the framebuffer.
				</code></pre>

				<p class="tab">Since a SPRT_16 primitive does not have its own <code>tpage</code> member, our sprite must use the <i>current</i> texture page. DR_MODE allows us to change the current texture page to whatever we specify like this:</p>
				<pre><code>
DR_MODE dr_mode;

// void SetDrawMode( DR_MODE *p     pointer to drawing mode primitive
//                   int dfe,       allow of drawing in display area
//		     int dtd,       dithering
//		     int tpage,     texture page
//		     RECT *tw)      pointer to texture window

SetDrawMode(&amp;dr_mode, 0, 0, LoadTPage(ball16x16, 0, 0, 384, 0, 16, 16), 0); // set current texture page
				</code></pre>

				<p class="tab">For this to work however, we must execute the DR_MODE primitive first, and then execute the SPRT_16 primitive.</p>
				<h3>Putting it together:</h3>

				<pre><code>
POLY_FT4 ft4;
SPRT_16 spr;
DR_MODE dr_mode;

main()
{
	LoadClut2(ballcolor[0], 384, 32);

	// Initialize dr_mode
	SetDrawMode(&amp;dr_mode, 0, 0, LoadTPage(ball16x16, 0, 0, 384, 0, 16, 16), 0);

	// Initialize sprite
	SetSprt16(&amp;spr);
	setRGB0(&amp;spr, 100, 100, 100);
	setXY0(&amp;spr, 145, 120);
	setUV0(&amp;spr, 0, 0);
	spr.clut = GetClut(384, 32);

	// Initialize quad
	SetPolyFT4(&amp;ft4);
	setRGB0(&amp;ft4, 100, 100, 100); // Set brightness
	setXY4(&amp;ft4, 200, 100, 300, 100, 200, 200, 300, 200) // top left -&gt; top right -&gt; bottom left -&gt; bottom right
	ft4.tpage = LoadTPage(ball16x16, 0, 0, 384, 0, 16, 16); // Load texture into framebuffer and return ID
	setUV4(&amp;ft4, 0, 0, 16, 0, 0, 16, 16, 16);
	ft4.clut = GetClut(384, 32);

	while(1)
	{
		DrawPrim(&amp;dr_mode);
		DrawPrim(&amp;spr); // spr uses the current texture page set by dr_mode primitive
		DrawPrim(&amp;ft4);
	}
}
				</code></pre>

				<div class="center">
					<img src="../../images/blog/spr_and_ft4.png" alt="image of SPRT_16 and POLY_FT4">
				</div>

				<p class="tab">Within the Run-Time Library Reference file (LIBREF46.PDF), there were mentions of documentation on a sprite editor, but sadly I didn't find anything. I may be able to render a textured primitive, but I have no idea how to create a texture.</p>

				<div class="center">
					<img src="../../images/blog/sprite_editor.png" alt="image of sprite_editor reference" style="border:solid;width:90%;height:90%;">
				</div>

				<h2>Conclusion</h2>
				<p class="tab">Between this entry and the last entry, I had discovered the demoscene. These reverse-engineering gods have made demos for all kinds of consoles, and this culture serves as a major inspiration for me to continue homebrew development. Particularly, PC demos from the early 2000s and late 1990s have influenced me the most, and as a result my first application will probably be a simple audiovisual demo instead of a game. Either way, I still have to trek against the exciting yet arduous path to develop for this system, and I will continue to crack at it for years to come. Thanks for reading.</p>
				
			</div>
		</main>
	</body>
</html>
