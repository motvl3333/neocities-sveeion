<!DOCTYPE html>
<html>
	<head>
		<title>03 - Anatomy of a sprite</title>
		<link rel="stylesheet" href="../../style.css">
		<link rel="icon" href="../../cube.ico">
	</head>

	<body>
		<header>
			<h1>03 - Anatomy of a sprite</h1>

			<div class="menu">
				<a href="/">Home</a>
				<a href="../../about.html">About</a>
				<a href="../../blog.html">Back</a>
			</div>
			<hr>
		</header>

		<main>
			<h6>March 18, 2019</h6>
			<div class="off_of_margin">
				<p class="tab">A couple weeks ago I was struggling to create a sprite, but I finally found out how to do it after manipulating pre-made textures in the Psy-Q sample programs. In this entry, I will thoroughly describe how you can manually create a sprite.</p>
				<div class="center">
					<img src="../../images/blog/starcircle.gif" alt="image of a simple demo using custom sprites. A yellow star is placed in the center, while four purple sprites circulate the star.">
				</div>

				<h2>Intro</h2>
				<p class="tab">A texture can be in 3 distinct pixel modes: <b>4-bit</b>, <b>8-bit</b>, and <b>16-bit</b>.</p>
				<h3>16-bit textures</h3>
				<p class="tab">A texture in 16-bit mode means that each texture pixel (a.k.a. texel) is determined through 16 binary digits. 1 bit is used to determine semi-transparency, 5 bits are used for the blue value, 5 bits for green, and 5 bits for red:</p>

				<div class="center">
					<img src="../../images/blog/16-bit_pixel.png" alt="image of a 16-bit pixel represented through binary digits: 0 00000 00000 00000">
				</div>

				<p class="tab">If I wanted to have an opaque red texel, I would fill in the first five bits to one:</p>

				<div class="center">
					<img src="../../images/blog/16-bit_pixel_red.png" alt="image of a red 16-bit pixel represented through binary digits: 0 00000 00000 11111">
				</div>

				<p class="tab">We can represent <code>0 00000 00000 11111</code> via hexadecimal. For clarity, the highest value a hexadecimal digit can represent is F, so how many bits would you need to represent a hexadecimal digit..? <span class="spoiler">Four</span> binary digits are needed to represent a hexadecimal digit. Knowing this, we can rearrange <code>0 00000 00000 11111</code> to <code>0000 0000 0001 1111</code>. This means that a 16-bit texture pixel can be represented with four hexadecimal digits. We denote a hexadecimal digit in C by writing '0x' before the value:</p>

				<div class="center">
					<img src="../../images/blog/16-bit_pixel_hex.png" alt="image of 0000 0000 0001 1111 in binary being represented as 0 0 1 f in hexadecimal.">
				</div>

				<p class="tab">An important thing to note here is that <b>we always represent texels with 8 hexadecimal digits, no matter the mode.</b> If I wanted to manually create a 16x16px texture in either 4-bit, 8-bit, or 16-bit mode, I'd declare an <code>unsigned long</code> array and fill the array with 8-digit hex values. Since we have 8 hex digits, in 16-bit mode we'll be able to hold two 16-bit texels. For example, if I wanted to have my first two texels to be red, I'd declare:</p>
				<pre><code>
// 16-bit mode texture
unsigned long texture[] = {
	0x001f001f
};
				</code></pre>

				<p class="tab">If I wanted the whole top row of texels in a 16x16 texture to be red, I'd declare:</p>

				<pre><code>
unsigned long texture[] = {
	0x001f001f, 0x001f001f, 0x001f001f, 0x001f001f,
	0x001f001f, 0x001f001f, 0x001f001f, 0x001f001f 
};
				</code></pre>

				<div class="center">
					<img src="../../images/blog/redline_tex.PNG" alt="an image of a 16x16 grid with the top row being filled with red.">
				</div>

				<p class="tab">I'll skip over 8-bit textures and jump to 4-bit textures:</p>

				<h3>4-bit textures</h3>
				<p class="tab">A texture in 4-bit mode means that each texel is determined through 4 binary digits (1 hex digit). The whole texture can only use 16 entries within any CLUT, and the value of a single hex digit is used to determine an entry within the CLUT for a single texel. Let's say we wanted a 16x16px texture that had its top row of texels be red and its second row be blue. First, we'd declare colors within a CLUT. <b>The colors within a CLUT are determined by using 16-bits, identical to what we did above with 16-bit textures.</b> Then we'd create the texture itself:</p>

				<pre><code>
unsigned long clut[] = {
	0x7c00001f // have 0th entry be red, and 1st entry be blue. every other entry is transparent
};

// each hex digit represents a texel
unsigned long texture[] = {
	0x00000000, 0x00000000, // row 1
	0x11111111, 0x11111111  // row 2
};
				</code></pre>

				<div class="center">
					<img src="../../images/blog/redline_blueline_tex.PNG" alt="an image of a 16x16 grid with the top row being filled with red, and the second row being filled with blue.">
				</div>

				<p class="tab">If we wanted to draw this:</p>
				<div class="center">
					<img src="../../images/blog/sprite_face.png" alt="an image of crudely drawn face on a 16x16 grid.">
				</div>

				<p class="tab">I would first declare a CLUT with four entries: the first entry would be a transparent value (0x0000) while the other three would be red (0x001f), green (0x03e0), and blue (0x7c00). Next, I would create the texture itself and use the value of each digit as an entry to the CLUT:</p>
				<pre><code>
unsigned long clut[] = {
	0x001f0000, 0x7c0003e0
};

unsigned long texture[] = {
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000,
	0x02222200, 0x00011110,
	0x02000200, 0x00010010,
	0x02000200, 0x00010010,
	0x02220200, 0x00010110,
	0x00022200, 0x00011100,
	0x00000000, 0x00000000,
	0x00003000, 0x00003000,
	0x00033000, 0x00003300,
	0x33330000, 0x00000333,
	0x00300000, 0x00000030,
	0x33000000, 0x00000003,
	0x00000000, 0x00000000,
	0x00000000, 0x00000000
};
				</code></pre>

				<p class="tab">All you'd have to do now is apply the texture and CLUT to a primitive and we're done!</p>
				<h2>Conclusion</h2>
				<p class="tab">Very recently, I figured out how to receive inputs from a standard controller, and now I am developing a neat little test game. It's a simple platformer, and I plan to post it on GitHub when it's ready in about a month or two. In the last entry I said I was going to create an audiovisual demo, but receiving input was much more exciting since I originally wanted to create games. Once this little test game is finished, I'll work on a slightly bigger game, taking what I learned from the test game into account. Kirby Nightmare in Dreamland serves as the main inspiration for the type of game I eventually want to make.</p>

			</div>
		</main>
	</body>
</html>
