<!DOCTYPE html>
<html>
	<head>
		<title>01 - Rendering primitives</title>
		<link rel="stylesheet" href="../../style.css">
		<link rel="icon" href="../../cube.ico">
	</head>

	<body>
		<header>
			<h1>01 - Rendering primitives</h1>

			<div class="menu">
				<a href="/">Home</a>
				<a href="../../about.html">About</a>
				<a href="../../blog.html">Back</a>
			</div>
			<hr>
		</header>

		<main>
			<h6>Feburary 9, 2019</h6>
			<div class="off_of_margin">
				<p class="tab">In this entry, I will be describing how I was able to render multiple primitives on the PlayStation (using emulators). Before writing any code (language being C), I <span style="text-decoration:line-through">extensively</span> read documentation on the hardware of the PlayStation and the Basic Graphics Library included with Psy-Q. An online overview of the PlayStation's technical specifications can be found on <a href="https://en.wikipedia.org/wiki/PlayStation_technical_specifications">Wikipedia</a> or <a href="http://www.psxdev.net/forum/viewtopic.php?f=47&t=21">PSXDEV</a>. I will only give a brief overview of the graphics system here.</p>

				<div class="center">
					<img src="../../images/blog/main.png" style="width:90%;height:90%;">
					<h5>Mednafen running MAIN.EXE</h5>
				</div>

				<h2>The Graphics System</h2>
				<p>The PlayStation's Graphics System is composed of three parts: the GTE (Geometry Transform Engine), GPU (Graphics Processing Unit), and the framebuffer.</p>

				<h3>The GTE</h3>
				<p class="tab">The PlayStation's GTE mainly deals with calculating vectors, light source data, and polygons for the GPU to use. I haven't done much reading here since my main focus these past weeks were on utilizing the GPU.</p>

				<h3>The GPU</h3>
				<p class="tab">The PlayStation's GPU renders the graphics! It supports many resolutions, the lowest being 256x224 (Progressive) and the highest being 640x480 (Interlace). If a game has an inventory screen, the developers may choose to have the inventory screen run at a higher resolution such as 640x480, while the gameplay remains at a lower resolution such as 320x240. Only a few titles run at 640x480 in-game.</p>

				<h3>The Framebuffer</h3>
				<p class="tab">The PlayStation's GPU contains 1MB of VRAM which entirely consists of the framebuffer. The framebuffer is a 1024x512 bitmap that contains the display area, drawing area, sprite patterns, and CLUTs (Color Look Up Tables). A double buffering system is used to alternate two buffers present in the display area (finished frame shown to screen) and the drawing area (frame being updated).</p>

				<div class="center">
					<img src="../../images/blog/mitmw_vram.png" style="width:90%;height:90%">
					<h5>Viewing the framebuffer while running <a href="http://www.psxdev.net/forum/viewtopic.php?t=572">Marilyn: In the Magic World</a></h5>
				</div>

				<h2>Primitives</h2>
				<p class="tab">A primitive is the smallest unit that the graphics system can handle. There is a variety of primitives to choose from, but here are the ones I used:</p>

				<table align="center">
					<tr>
						<th>Name</th>
						<th>Description</th>
					</tr>

					<tr>
						<td>POLY_F4</td>
						<td>A four sided polygon that uses flat shading.</td>
					</tr>
					<tr>
						<td>POLY_G3</td>
						<td>A three sided polygon that uses Gouraud shading.</td>
					</tr>
					<tr>
						<td>LINE_F2</td>
						<td>A straight line.</td>
					</tr>
					<tr>
						<td>LINE_G2</td>
						<td>A straight line with color gradation.</td>
					</tr>
				</table>
				<h2>Ordering Tables</h2>
				<p class="tab">An ordering table is a linked list that holds multiple primitives. To create one, you'd write:</p>

				<code>unsigned long ot[OTSIZE];</code> <br>
				<code>ClearOTag(ot); //Initialize the OT</code> <br>

				<p class="tab">In order to add primitives into the ordering table, we would use <code>AddPrim()</code> with the first argument being the ordering table entry, and the second being the primitive in question. Lower ordering table entries (such as ot[0], ot[1], etc..) get executed first onto the screen while higher entries (such as ot[OTSIZE-2], ot[OTSIZE-1]) get executed last. For example, if I add a primitive in ot[3] and a primitive at ot[100], any primitives in ot[3] would get drawn to the screen first. To execute an ordering table, I would use the <code>DrawOTag()</code> function with <code>ot</code> as an argument. Note that it is possible to add multiple primitives into one ordering table entry.</p>

				<p class="tab">With all of this in mind, creating a simple application that had a few moving primitives was straightforward.</p>

				<h2>Writing stuff</h2>
				<p class="tab">All primitives in Psy-Q are defined as C structures. As a result, you are able to modify the members of all primitives directly using the dot operator, or by using a function. For example, if I declare a <code>LINE_F2</code>:</p>

				<code>LINE_F2 f2;</code>
				
				<p>and want to modify the members <code>x0</code> and <code>y0</code>, I could say:</p>


				<pre><code>
f2.x0 = 100;
f2.y0 = 30;
				</code></pre>


				<p>or</p>

				<code>setXY(&amp;f2, 100, 30);</code>

				<p class="tab">Note that modifying the <code>x0</code> and <code>y0</code> values will only modify one point on the line. Since a line has two points, there are actually four values that determine the position of a line: <code>x0</code>, <code>y0</code>, <code>x1</code>, and <code>y1</code>. The corresponding function to set two points of a primitive is <code>setXY2()</code>. Before going any further, it's important that we initialize our primitives. To initialize a <code>LINE_F2</code> primitive, we use <code>SetLineF2()</code> with the memory address of <code>f2</code> as our argument like so:</p>

				<code>SetLineF2(&amp;f2);</code>

				<p class="tab">Moving on, I wanted to create a custom primitive that was just a box with no fill, so I started out by creating a structure using four <code>LINE_F2</code> primitives:</p>

				<pre>
					<code>
typedef struct{
	LINE_F2 s0, s1, s2, s3;
}BOX;
					</code>
				</pre>

				<p class="tab">Next, I created a function named <code>SetBox()</code>. Its main purpose is to initialize my custom primitive named <code>BOX</code>. To do so however, it needs to initialize all of its child primitives (in this case it's just four <code>LINE_F2</code> primitives), set the location of each child primitive, and combine all child primitives together using <code>MargePrim()</code>.</p>
				<pre><code>
void SetBox(BOX *p, short x, short y, short size)
{
	// Initialization of child primitives
	SetLineF2(&amp;p-&gt;s0);
	SetLineF2(&amp;p-&gt;s1);
	SetLineF2(&amp;p-&gt;s2);
	SetLineF2(&amp;p-&gt;s3);

	// Set location of lines.
	setXY2(&amp;p-&gt;s0, x     ,      y, x+size, y);
	setXY2(&amp;p-&gt;s1, x+size,      y, x+size, y+size);
	setXY2(&amp;p-&gt;s2, x+size, y+size, x,      y+size);
	setXY2(&amp;p-&gt;s3, x     , y+size, x,      y);
	
	// Combine line primitives
	MargePrim(&amp;p-&gt;s0, &amp;-&gt;s1);
	MargePrim(&amp;p-&gt;s0, &amp;-&gt;s2);
	MargePrim(&amp;p-&gt;s0, &amp;-&gt;s3);

}
				</code></pre>

				<p class="tab">So if I were to write the following code:</p>
				<pre><code>
BOX b1;
SetBox(&amp;b1, 200, 300, 10);
				</code></pre>
				<p class="tab">It would set a box at the coordinates (200, 300) while making each side of the box 10 units long. Note that I can now use <code>b1</code> just like any other primitive. My resulting box would look something like this:</p>
				<div class="center">
					<img src="../../images/blog/box_sides.png" style="width:50%;height:50%;">
				</div>

				<p class="tab">To set up the other primitives such as <code>LINE_G2</code>, <code>POLY_G3</code>, and <code>POLY_F3</code>, the process is similar to <code>LINE_F2</code>'s. After all of the primitives have been initialized and modified, I add them to an ordering table using <code>AddPrim()</code>, with my first argument being an entry in the ordering table, and the second being the memory address of my primitive:</p>
				<pre><code>
AddPrim(ot[0], &amp;prim); // Register prim into the initial ordering table entry.  
				</code></pre>
				<p class="tab">After a bit of messing around with other primitives and changing their unique properties, I've created a little demo that utilizes <code>POLY_F4</code>, <code>POLY_G3</code>, <code>LINE_F2</code>, and <code>LINE_G2</code>. Since I don't want to host my videos on YouTube or host any files on Dropbox anymore, here's a short gif:</p>

				<div class="center">
					<img src="../../images/blog/main_exe_anim_short.gif" style="height:90%;width:90%;">
				</div>

				<h2>Conclusion</h2>
				<p class="tab">At this point I've become a bit obsessed with the system. The PlayStation had mostly appealed to me by how its games looked and how badass the startup sequence was, but now it's more about creating an application that maximizes the system's potential. It's not like a game engine where your main concern is game design, it's more about learning a system's internals and controlling how they operate. I must say that the development of Crash Bandicoot was single-handedly the most interesting thing I read while learning about the PlayStation. The tricks and loops Naughty Dog went through to develop that game is mind-boggling, and that is what I want out of game development!</p>
			</div>
		</main>
	</body>
</html>
